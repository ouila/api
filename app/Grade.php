<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    public function students(){
        return $this->belongsToMany('App\User', 'grade_user');
    }

    public function courses(){
        return $this->belongsToMany('App\Course', 'grade_course');
    }
}
