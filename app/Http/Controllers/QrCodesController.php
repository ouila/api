<?php

namespace App\Http\Controllers;

use Auth;
use QrCode;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GenerateQrCode;
use Log;
class QrCodesController extends Controller
{
    private $PATH = '/img/qr-code/';
    public function qrcode()
    {
        if ($this->isAdmin()) {
            $timestamp = time();

            $img = QrCode::format('png')->size(512)->generate(time());

            $save_path = $this->PATH.'img' . $timestamp . '.png';
            $this->cleanImg();
            Storage::disk('local')->put($save_path, $img);
            $base64 = 'data:image/png;base64,'.base64_encode($img);
            return response()->json(['qrcode' => $base64]);
        } else {
            return [
                'error' => 'true',
                'message' => 'Not Authorized'
            ];
        }
    }

    function isAdmin()
    {
        $user = Auth::user();
        return $user->admin;
    }
    private function generateQrCode(){
        $job = (new GenerateQrCode(true));
        $this->dispatch($job);
    }
    private function cleanImg(){
        $files = Storage::disk('local')->allFiles($this->PATH);
        foreach($files as $file) {
            Storage::delete($file);
        }
    }
}
