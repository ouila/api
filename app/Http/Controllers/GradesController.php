<?php

namespace App\Http\Controllers;

use Auth;
use App\Grade;
use Illuminate\Http\Request;

class GradesController extends Controller
{
    public function show($id)
    {
        if ($this->isAdmin()) {
            $grade = Grade::with('students')->findOrFail($id);
            return ['grade', $grade];
        } else {
            return [
                'error' => 'true',
                'message' => 'Not Authorized'
            ];
        }
    }

    function isAdmin()
    {
        $user = Auth::user();
        return $user->admin;
    }
}
