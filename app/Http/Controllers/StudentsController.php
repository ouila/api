<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;


class StudentsController extends Controller
{
    /**
     * Only if Admin
     */
    public function show($id)
    {
        $student = User::with('grades')->with('courses')->findOrFail($id);
        return ['student', $student];
    }
}
