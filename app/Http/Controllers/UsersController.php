<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Log;
use PHPUnit\Framework\Constraint\Exception;
use Illuminate\Support\Facades\Storage;
use App\Events\PresenceUser;

class UsersController extends Controller
{
    /** 
     * login api with the email and password
     * 
     * @return \Illuminate\Http\Response 
     */
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = $this->getRelations(Auth::user());
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $message = null;
            if (($user->imei != null) && ($user->imei != $request->input('imei') && ($request->input('imei') != null))) {
                $message = "Attention l'identifiant <b>unique</b> de votre téléphone pour ce compte va être remplacé !";
                $user->setImei($request->input('imei'));
                $user->save();
            }
            if (($user->mac != null) && ($user->mac != $request->input('mac') && ($request->input('mac') != null))) {
                $message .="\n Attention l'identifiant <b>réseau</b> de votre téléphone pour ce compte va être remplacé !";
                $user->setMac($request->input('mac'));
                $user->save();
            }
            return response()->json(['success' => $success, 'user' => $user, 'message', 'message' => $message], 200);
        } else {
            return response()->json(['error' => 'Wrong username or password'], 200);
        }
    }

    /**
     * Login with the mac address
     */
    public function login_imei(Request $request)
    {
        $user = User::where('imei', '=', $request->input('imei'))->first();
        if (empty($user)) {
            return response()->json(['error' => 'Wrong username or password'], 200);
        } else {
            $user = $this->getRelations($user);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success, 'user' => $user], 200);
        }
    }

    public function logout(){
        Log::debug("Hello World");
        $user = Auth::user()->token();
        $user->revoke();
        return response()->json(['logged out']);
    }
    
    public function presentBluetooth(Request $request){
        $user_raspberry = Auth::user();
        if ($user_raspberry->admin == 2){
            $array = $request->input('mac_address');
            foreach($array as $value) {
                $mac = json_decode(json_encode($value))->mac;
                $user = User::where('mac', '=', $mac)->where('admin', '=', 0)->first();
                if(empty($user)) {
                    return response()->json(['error' => 'No user found'], 200);
                } else {
                    $user->setPresent(1);
                    $user->save();
                    broadcast(new PresenceUser($user));
                }
            }
            return response()->json('Users updated',200);
        } else {
            return response()->json('Access denied',200);
        }
    }

    public function presentQr(Request $request){
        $user = Auth::user();
        if ((!$user->admin)){
            //Check if QrCode has been generated
            if (Storage::disk('local')->has('/img/qr-code/img' . $request->input('code') . '.png')){
                $user->setPresent(1);
                $user->save();
                broadcast(new PresenceUser($user));
                return response()->json(['error' => false, 'message' => 'User updated'],200);   
            } else {
                return response()->json(['error' => true, 'message' =>'Not Authorized'], 200);
            }
        } else {
            return response()->json(['error' => true, 'message' =>'Not Authorized'], 200);
        }
    }


    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lastname' => 'required',
            'firstname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'admin' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 200);
        } else {
            //Register Student
            if ($request->input('admin') == 0) {
                //Check if imei already exists
                $check_user = User::where('imei', '=', $request->input('imei'))->orWhere('mac', '=', $request->input('mac'))->first();
                if (empty($check_user)) {
                    $input = $request->all();
                    $input['password'] = bcrypt($input['password']);
                    $user = User::create($input);
                    $user = $this->getRelations($user);
                    $success['token'] =  $user->createToken('MyApp')->accessToken;;
                    return response()->json(['success' => $success, 'user' => $user], 200);
                } else {
                return response()->json(['error' => 'IMEI or MAC already exists'], 200);
                }   
            } 
            //Register Teacher
            else if (($request->input('admin') == 1) || ($request->input('admin') == 2)) {
                $input = $request->all();
                $input['password'] = bcrypt($input['password']);
                $user = User::create($input);
                $user = $this->getRelations($user);
                $success['token'] =  $user->createToken('MyApp')->accessToken;;
                return response()->json(['success' => $success, 'user' => $user], 200);
            } else {
                return response()->json(['error' => 'Make sure to correctly fill the form'], 200);
            }
        }
    }

    function getRelations($user)
    {
        if ($user->admin) {
            $user->courses;
        } else {
            $user->grades;
        }
        return $user;
    }
}
