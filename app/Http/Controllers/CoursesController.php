<?php

namespace App\Http\Controllers;

use Auth;
use App\Course;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function show($id)
    {
        if ($this->isAdmin()) {
            $course = Course::with('grades')->findOrFail($id);
            return ['course', $course];
        } else {
            return [
                'error' => 'true',
                'message' => 'Not Authorized'
            ];
        }
    }

    function isAdmin()
    {
        $user = Auth::user();
        return $user->admin;
    }
}
