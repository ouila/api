<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function teacher(){
        return $this->hasOne('App\User');
    }

    public function grades(){
        return $this->belongsToMany('App\Grade', 'grade_course');
    }
}