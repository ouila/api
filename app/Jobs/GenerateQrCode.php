<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Events\NewMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use QrCode;
use Illuminate\Support\Facades\Storage;
use Log;
class GenerateQrCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $continue = true;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($continue)
    {
        $this->$continue = $continue;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    }
}
