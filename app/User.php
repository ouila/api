<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lastname', 'firstname', 'email', 'password', 'notification_key', 'admin', 'imei', 'mac', 'present'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'notification_key', 'imei', 'mac'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setImei($imei)
    {
        $this->attributes['imei'] = $imei;
    }

    public function setMac($mac)
    {
        $this->attributes['mac'] = $mac;
    }

    public function setPresent($present)
    {
        $this->attributes['present'] = $present;
    }

    public function grades()
    {
        return $this->belongsToMany('App\Grade', 'grade_user');
    }

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function scopeStudent($query)
    {
        return $query->where('admin', 0)->get();
    }
}
