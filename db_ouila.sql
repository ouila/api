-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 02 sep. 2019 à 23:16
-- Version du serveur :  10.1.37-MariaDB
-- Version de PHP :  7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_ouila`
--

-- --------------------------------------------------------

--
-- Structure de la table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `duration` datetime NOT NULL,
  `allClass` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `courses`
--

INSERT INTO `courses` (`id`, `label`, `start_time`, `duration`, `allClass`, `created_at`, `updated_at`, `user_id`) VALUES
(21, 'ut', '1982-11-15 18:32:52', '1975-03-11 03:42:15', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 264),
(22, 'illum', '1995-11-12 06:41:03', '1997-08-12 12:13:48', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 273),
(23, 'facilis', '1981-01-14 07:10:06', '1995-04-30 16:40:58', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 265),
(24, 'quam', '1999-09-06 14:09:15', '1999-09-17 04:23:34', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 268),
(25, 'neque', '1999-02-20 17:03:58', '2005-08-22 04:21:19', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 271),
(26, 'omnis', '1993-09-13 06:14:06', '2003-03-08 13:45:12', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 270),
(27, 'atque', '1982-11-29 03:46:17', '1977-01-13 06:59:06', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 273),
(28, 'labore', '2005-12-19 04:57:48', '1991-10-29 13:28:37', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 272),
(29, 'qui', '2005-10-02 23:45:48', '1975-09-16 11:37:58', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 266),
(30, 'voluptatem', '1994-03-01 05:20:21', '1997-04-02 07:38:26', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 266),
(31, 'voluptatem', '1997-08-01 01:58:38', '2003-05-11 11:51:36', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 267),
(32, 'ea', '1989-12-05 11:41:39', '1971-01-16 10:59:54', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 265),
(33, 'perferendis', '1985-12-01 19:47:19', '1984-05-29 17:54:28', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 265),
(34, 'suscipit', '2009-02-21 01:22:02', '2012-12-16 18:25:02', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 273),
(35, 'praesentium', '1982-12-13 06:43:17', '1970-08-18 12:18:11', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 272),
(36, 'sit', '2019-06-19 08:25:39', '1971-12-29 22:30:14', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 265),
(37, 'distinctio', '2010-06-30 16:28:03', '2012-03-21 02:11:41', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 269),
(38, 'alias', '2001-12-13 15:25:21', '1982-07-25 11:28:28', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 268),
(39, 'quia', '1991-09-25 01:27:29', '1978-03-14 14:11:05', 1, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 267),
(40, 'et', '1984-09-03 20:37:10', '1988-05-20 00:53:37', 0, '2019-06-29 08:45:30', '2019-06-29 08:45:30', 268);

-- --------------------------------------------------------

--
-- Structure de la table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `grades`
--

INSERT INTO `grades` (`id`, `label`, `created_at`, `updated_at`) VALUES
(1, 'qui', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(2, 'illo', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(3, 'sint', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(4, 'accusantium', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(5, 'vitae', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(6, 'dolorem', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(7, 'ut', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(8, 'distinctio', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(9, 'deserunt', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(10, 'dignissimos', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(11, 'consequuntur', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(12, 'eius', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(13, 'magnam', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(14, 'illum', '2019-06-28 19:54:26', '2019-06-28 19:54:26'),
(15, 'vel', '2019-06-28 19:54:26', '2019-06-28 19:54:26');

-- --------------------------------------------------------

--
-- Structure de la table `grade_course`
--

CREATE TABLE `grade_course` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `grade_course`
--

INSERT INTO `grade_course` (`id`, `grade_id`, `course_id`, `created_at`, `updated_at`) VALUES
(2, 5, 21, NULL, NULL),
(3, 3, 22, NULL, NULL),
(4, 2, 35, NULL, NULL),
(5, 10, 40, NULL, NULL),
(6, 1, 21, NULL, NULL),
(7, 6, 21, NULL, NULL),
(8, 4, 32, NULL, NULL),
(9, 9, 26, NULL, NULL),
(10, 12, 23, NULL, NULL),
(11, 8, 34, NULL, NULL),
(12, 15, 28, NULL, NULL),
(13, 14, 30, NULL, NULL),
(14, 7, 38, NULL, NULL),
(15, 11, 38, NULL, NULL),
(16, 13, 40, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `grade_user`
--

CREATE TABLE `grade_user` (
  `grade_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `grade_user`
--

INSERT INTO `grade_user` (`grade_id`, `user_id`, `created_at`, `updated_at`) VALUES
(15, 299, NULL, NULL),
(11, 293, NULL, NULL),
(6, 280, NULL, NULL),
(6, 292, NULL, NULL),
(9, 297, NULL, NULL),
(14, 285, NULL, NULL),
(3, 301, NULL, NULL),
(8, 296, NULL, NULL),
(3, 284, NULL, NULL),
(9, 302, NULL, NULL),
(14, 275, NULL, NULL),
(12, 293, NULL, NULL),
(11, 277, NULL, NULL),
(2, 280, NULL, NULL),
(10, 300, NULL, NULL),
(10, 279, NULL, NULL),
(14, 277, NULL, NULL),
(3, 301, NULL, NULL),
(1, 303, NULL, NULL),
(8, 278, NULL, NULL),
(3, 275, NULL, NULL),
(15, 286, NULL, NULL),
(11, 303, NULL, NULL),
(12, 293, NULL, NULL),
(1, 295, NULL, NULL),
(3, 296, NULL, NULL),
(3, 299, NULL, NULL),
(14, 290, NULL, NULL),
(12, 297, NULL, NULL),
(2, 285, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_28_173720_create_grades_table', 1),
(4, '2019_06_28_174026_create_courses_table', 2),
(6, '2019_06_28_181755_update_name_users_table', 4),
(8, '2019_06_28_182310_add_firstname_to_users_table', 5),
(9, '2019_06_28_183430_add_notification_key_to_users_table', 6),
(11, '2019_06_28_183948_add_admin_to_users_table', 7),
(15, '2019_06_28_220741_remove_id_grade_user_table', 9),
(16, '2019_06_28_222023_add_present_to_users_table', 10),
(17, '2016_06_01_000001_create_oauth_auth_codes_table', 11),
(18, '2016_06_01_000002_create_oauth_access_tokens_table', 11),
(19, '2016_06_01_000003_create_oauth_refresh_tokens_table', 11),
(20, '2016_06_01_000004_create_oauth_clients_table', 11),
(21, '2016_06_01_000005_create_oauth_personal_access_clients_table', 11),
(22, '2019_06_28_225737_update_notification_key_users_table', 12),
(23, '2019_06_28_225937_update_admin_users_table', 13),
(32, '2019_06_28_180006_create_grade_course_table', 14),
(33, '2019_06_28_192357_create_grade_user_table', 14),
(34, '2019_06_29_103856_add_user_id_to_course_table', 14),
(35, '2019_07_01_140653_create_websockets_statistics_entries_table', 15),
(43, '2019_07_03_153223_add_imei_number_to_users_table', 16),
(44, '2019_07_06_094043_add_mac_to_users_table', 16),
(45, '2019_08_20_172820_create_jobs_table', 16);

-- --------------------------------------------------------

--
-- Structure de la table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('01b8a564910556a1c3a6f5b52a2549642ba85c14135bcdfa71a33afa9baa111d1ae0bf95606a47d4', 321, 3, 'MyApp', '[]', 0, '2019-08-20 19:41:54', '2019-08-20 19:41:54', '2020-08-20 21:41:54'),
('09a28824bf1ac84d630275bcece53bb5830ec424ce8394b5bad89d64c1dd08afa3965bfe654e6956', 264, 1, 'MyApp', '[]', 0, '2019-06-28 21:44:12', '2019-06-28 21:44:12', '2020-06-28 23:44:12'),
('0aa5abf7af3ed1fed98f1d6583ecbe79f0c0c63c95e40c50aa9dc53e46eb49a63beba3f737e8734c', 264, 1, 'MyApp', '[]', 0, '2019-06-29 12:26:57', '2019-06-29 12:26:57', '2020-06-29 14:26:57'),
('0acd82799c3ddf7946886a022711c27fa7717daa8e1ce0d31b605bbe298d9060a5a350262cb1b167', 264, 3, 'MyApp', '[]', 0, '2019-08-20 20:06:37', '2019-08-20 20:06:37', '2020-08-20 22:06:37'),
('0c5ec19287cc81cf04f19c251a31e1a3cf1287f0d60f6a08b9a254e650aef61cf582976227249d86', 282, 1, 'MyApp', '[]', 0, '2019-06-29 12:52:58', '2019-06-29 12:52:58', '2020-06-29 14:52:58'),
('10935dec041fe2904ebec924bdc43000a8acbbdd6e32701ab0035b56027fc034f41a7dbeb1523e81', 321, 3, 'MyApp', '[]', 0, '2019-08-20 19:43:12', '2019-08-20 19:43:12', '2020-08-20 21:43:12'),
('11075d23a761b79c47aa27a010244cc21577904622d187e1b6adac7f25be04b96d6a7deca68a2477', 264, 3, 'MyApp', '[]', 0, '2019-08-20 20:07:17', '2019-08-20 20:07:17', '2020-08-20 22:07:17'),
('125ce77a73a6e02801e3efbb7666bc8cd199ce1d027c9d7dc369da3b3b2378d9c4f0039be69197ba', 264, 3, 'MyApp', '[]', 0, '2019-07-27 11:10:55', '2019-07-27 11:10:55', '2020-07-27 13:10:55'),
('1319ce1498c9d3c5aa1ffa398f48db30405ba45ad421eb5bf7d334a1159e3a04a78e041182bac91b', 321, 3, 'MyApp', '[]', 0, '2019-08-21 18:45:11', '2019-08-21 18:45:11', '2020-08-21 20:45:11'),
('1e2776e1b75065268b6f4d54a638b1079de05be536dbc86b1ba8699e3791383ce5f3e11b677b044b', 264, 3, 'MyApp', '[]', 0, '2019-07-03 21:49:49', '2019-07-03 21:49:49', '2020-07-03 23:49:49'),
('1f5483efb381db4256893d860b887836dd82351132b3ee05208fae3dcf9e435b6e62f21eed175a7f', 214, 1, 'MyApp', '[]', 0, '2019-06-28 21:38:43', '2019-06-28 21:38:43', '2020-06-28 23:38:43'),
('2227a6e2f99dd55e9fed6687688b4fb116f21a5e080e630a3bba24eab3ce2547f5820f3eb31f0851', 122, 1, 'MyApp', '[]', 0, '2019-06-28 21:17:07', '2019-06-28 21:17:07', '2020-06-28 23:17:07'),
('24449f0e2324999a8ec649685fb79a4299dcef44b94d62a03906601cc35ac4b29b05af86e97b1664', 295, 3, 'MyApp', '[]', 0, '2019-08-22 16:18:23', '2019-08-22 16:18:23', '2020-08-22 18:18:23'),
('2568f72a1b7fde06e0e5d97f96d9d514f58a53604e1da337b74aa90a27fbe53c68c9323e19849d28', 81, 1, 'MyApp', '[]', 0, '2019-06-28 21:03:56', '2019-06-28 21:03:56', '2020-06-28 23:03:56'),
('25f90e6a88921d9aef37d60cd49de6fed7a5cd0e5c51b190e7e0534e4aac25f12bec2b5300334327', 274, 1, 'MyApp', '[]', 0, '2019-06-28 22:26:02', '2019-06-28 22:26:02', '2020-06-29 00:26:02'),
('2bbf1a2bd21237c258bacf9fc071845259ebb744097d753396ee378bb7bd7d680a2ca5fdb788b334', 264, 3, 'MyApp', '[]', 0, '2019-07-03 21:44:18', '2019-07-03 21:44:18', '2020-07-03 23:44:18'),
('2be55d324b8d54f364575500fed8ec4e71cc93b6a0d1c1998b862ee7c43811002021dafbdebce047', 320, 3, 'MyApp', '[]', 0, '2019-07-06 09:59:58', '2019-07-06 09:59:58', '2020-07-06 11:59:58'),
('2f388a01a1c526d8f92cf41c02bf426c50ebf96c203d5b09ea0e6502180c144195fc90915f72f682', 264, 3, 'MyApp', '[]', 1, '2019-07-06 17:13:29', '2019-07-06 17:13:29', '2020-07-06 19:13:29'),
('2fe7ca824289239b3775e6f3759195d22edb47117a798d31dba1ecb2ea5ef286a00663cc3cb6ea7b', 307, 3, 'MyApp', '[]', 0, '2019-07-02 12:35:37', '2019-07-02 12:35:37', '2020-07-02 14:35:37'),
('30d7007be42e9d3db004739a0dcddc5b22becd9d43abecc3ffcf59688327923f68d6d31122bd7b2a', 264, 3, 'MyApp', '[]', 0, '2019-07-06 07:52:18', '2019-07-06 07:52:18', '2020-07-06 09:52:18'),
('31f2b8781216e10b86ca4de8777ff5d97deebb605b97ae2d33d9b86dcb1e384ec0408c9432838b3a', 264, 1, 'MyApp', '[]', 0, '2019-06-29 11:37:08', '2019-06-29 11:37:08', '2020-06-29 13:37:08'),
('326689fbbc698ec255f0a064e0c5cacad482d234ab27ddf766a1689c9d76028d222f75ae2e55c4bc', 264, 3, 'MyApp', '[]', 1, '2019-08-28 17:49:37', '2019-08-28 17:49:37', '2020-08-28 19:49:37'),
('328a085706933bef2b8ae0f32f58b945e5bc54a9608516bcee8e347c87baf5232949854388cabb85', 264, 3, 'MyApp', '[]', 0, '2019-07-06 16:17:11', '2019-07-06 16:17:11', '2020-07-06 18:17:11'),
('368383af5628709c22deb95fada240aaf44e9f331daf35ea2ea89271b76d704b1d6732f6b382749f', 318, 3, 'MyApp', '[]', 0, '2019-07-03 13:41:48', '2019-07-03 13:41:48', '2020-07-03 15:41:48'),
('3914d80191dec356c43ef7ef3507a9a1d96968ebe60ee4d045aa4f49fb2eaf12f455e00e01439b07', 264, 3, 'MyApp', '[]', 0, '2019-07-27 11:34:18', '2019-07-27 11:34:18', '2020-07-27 13:34:18'),
('3ad9a19a8d039d380b255438e6fd659d1ac825603079ffb9b4eadd21f40adad3be451215682ac52e', 264, 3, 'MyApp', '[]', 0, '2019-08-22 17:18:23', '2019-08-22 17:18:23', '2020-08-22 19:18:23'),
('3b3c5d664b390eaa45199ac14635001c02cdda4208d25756eddd1776bab9413d2fd7da3a6efd771c', 319, 3, 'MyApp', '[]', 0, '2019-07-05 17:18:48', '2019-07-05 17:18:48', '2020-07-05 19:18:48'),
('3ee1d926bd975938a75810dbb85a092950784a01258f743fb29d1f62b0b49b00e3043c389d09805e', 303, 3, 'MyApp', '[]', 0, '2019-08-28 18:15:53', '2019-08-28 18:15:53', '2020-08-28 20:15:53'),
('4709f825055ffa2043bb69b298ae551e239736e01321f2fc3876b74e218b0ad87ca165c39717aa7e', 264, 3, 'MyApp', '[]', 0, '2019-07-03 12:41:01', '2019-07-03 12:41:01', '2020-07-03 14:41:01'),
('48529da83b173a2edbccea5d48a92a7a3262089e688dd679bed889ee2888277014773519697dda43', 299, 1, 'MyApp', '[]', 0, '2019-06-29 12:53:23', '2019-06-29 12:53:23', '2020-06-29 14:53:23'),
('4d6b4f4570b7b1cc036c0d9763cc5bc02535ab50ad4bbcb3fb81d0e6da00ba20405a68c9f6d461bf', 264, 3, 'MyApp', '[]', 1, '2019-07-06 10:33:08', '2019-07-06 10:33:08', '2020-07-06 12:33:08'),
('5023f891921e24b27e4e1dbe187879c1d2afa2d5c4e3f8c3fe320f1557867fc0b409cab6f57e193a', 264, 3, 'MyApp', '[]', 0, '2019-07-03 21:48:21', '2019-07-03 21:48:21', '2020-07-03 23:48:21'),
('5039da43db714dfc7f87226bfe4f71d6314f88e8d7b513f2ed7503542129744c27e2b424924cb2e5', 264, 3, 'MyApp', '[]', 0, '2019-07-06 09:43:16', '2019-07-06 09:43:16', '2020-07-06 11:43:16'),
('514ef99b0c9b9efbc4915a3412e3f72fbe934ebb7c99676b90c57aee106570a0cbe444315d84aefb', 264, 1, 'MyApp', '[]', 0, '2019-06-29 12:54:45', '2019-06-29 12:54:45', '2020-06-29 14:54:45'),
('52e2a4bc0d54665850fdc59f1ab3a6f545401bca9ca9eaf4e5fe3a18c4610288fef905173b181281', 274, 3, 'MyApp', '[]', 0, '2019-07-27 11:46:44', '2019-07-27 11:46:44', '2020-07-27 13:46:44'),
('55cc52dbb6d7d96d14d2d8a4706a7f83fa723056fa3ce753e4aa1b1f1f7190b11d8cdf3854724d40', 264, 3, 'MyApp', '[]', 1, '2019-07-06 16:24:50', '2019-07-06 16:24:50', '2020-07-06 18:24:50'),
('5810d185f773660f06616942dc13ec825ef5df726e56efb27c0fcecca7f329b3abab83b6914844dc', 264, 3, 'MyApp', '[]', 1, '2019-07-06 16:20:20', '2019-07-06 16:20:20', '2020-07-06 18:20:20'),
('59fafbf13ccbdbbcb1aed4c162c6e42b29b5e65685d4d35e272dcb4ba4cbe24566922e7d0d42425c', 264, 3, 'MyApp', '[]', 0, '2019-07-06 07:10:00', '2019-07-06 07:10:00', '2020-07-06 09:10:00'),
('5de7e0df79d0c5edb3eaced05a3a2732558fc82f6bf1b21a51229a3bd29c3c4908104fae9710cb98', 203, 1, 'MyApp', '[]', 0, '2019-06-28 21:36:03', '2019-06-28 21:36:03', '2020-06-28 23:36:03'),
('5df1c2465c6379314872c6d71c0c63303b0bd66597eaef26c869f4255423722280378b762eb1f24f', 264, 3, 'MyApp', '[]', 0, '2019-08-20 16:01:33', '2019-08-20 16:01:33', '2020-08-20 18:01:33'),
('5fb928a6919c32921605b11db56a37b8a53eb53ac7d1dad469250dbc5bcf513149656628d39a850a', 320, 3, 'MyApp', '[]', 0, '2019-07-06 17:13:48', '2019-07-06 17:13:48', '2020-07-06 19:13:48'),
('60345e89867a303576e10200898fde5e9eabe96551dcd3f2ced85f19e6c7051667305745a8c8cab3', 308, 3, 'MyApp', '[]', 0, '2019-07-02 12:36:04', '2019-07-02 12:36:04', '2020-07-02 14:36:04'),
('6169e150b81f523be0fe328fa73c91ed3a2e01a43453e5470ea4d0bfc9a0fa0a72ca8472952ff3b2', 264, 1, 'MyApp', '[]', 0, '2019-06-29 11:39:27', '2019-06-29 11:39:27', '2020-06-29 13:39:27'),
('6192d52f7bab441010fc9a931ad5e76ac5e684a9c6f99801ef9fa43a6373322f13cbc0cc9d12e519', 266, 3, 'MyApp', '[]', 0, '2019-08-20 19:50:46', '2019-08-20 19:50:46', '2020-08-20 21:50:46'),
('61e3e6bba785f226823063991766436d74d6a23a0ba5a6d8ecc97eb06a6690d2b68a5819a8610bf3', 312, 3, 'MyApp', '[]', 0, '2019-07-02 12:57:00', '2019-07-02 12:57:00', '2020-07-02 14:57:00'),
('61faedb6ac1e7538f6c13a8c0825d183a274a2f8d1ef55db14ccf9ba96f6ee887c20058cdf8b42df', 264, 1, 'MyApp', '[]', 0, '2019-06-29 11:36:45', '2019-06-29 11:36:45', '2020-06-29 13:36:45'),
('620ffb64ed7e49eece1faa4ff927e12bc604de9bcd0285b7db60ce8703a14e4365a903e0c297f2e5', 302, 1, 'MyApp', '[]', 0, '2019-06-28 21:42:15', '2019-06-28 21:42:15', '2020-06-28 23:42:15'),
('62ddac5d6723eb92c556839925fe8f07ba9a533fce835ebeb994c2422af88ffef0ce9632580b41ad', 264, 3, 'MyApp', '[]', 0, '2019-07-03 12:42:09', '2019-07-03 12:42:09', '2020-07-03 14:42:09'),
('63146e70bb3f75ab60e7eee8192789f6b9fed0a73ff1c5f17b4e28ad2ca0eb4c2b72e2e31c0f9614', 264, 3, 'MyApp', '[]', 0, '2019-07-03 12:43:50', '2019-07-03 12:43:50', '2020-07-03 14:43:50'),
('646ef6529cd043f416bcd592586da8b6c35bc531682dd528c7ad6d84c5820366f22087befba1901f', 311, 3, 'MyApp', '[]', 0, '2019-07-02 12:51:49', '2019-07-02 12:51:49', '2020-07-02 14:51:49'),
('64ea5e1d803c54cfc9b3783ae0f293658274e481cd1b85b9cf413149a52db2fabac805f5bc9eeceb', 274, 3, 'MyApp', '[]', 0, '2019-08-20 15:53:04', '2019-08-20 15:53:04', '2020-08-20 17:53:04'),
('6668d49d00d598301fa698cb4ba4558fc0705b207f61c88ca09661be81a9dc88b5df1eb7b8f84fc0', 203, 1, 'MyApp', '[]', 0, '2019-06-28 21:34:47', '2019-06-28 21:34:47', '2020-06-28 23:34:47'),
('66a977e3fb3540e9197f61362041c083f4b0f43d76244a786b118189164384fcd35caba57ab081cc', 321, 3, 'MyApp', '[]', 0, '2019-08-20 19:56:14', '2019-08-20 19:56:14', '2020-08-20 21:56:14'),
('6a8633e4996552d39077f1d7c1ca90f166ea11a72f7e363ad8d7f597f24a3f4911aa73a3391f1ad8', 264, 3, 'MyApp', '[]', 0, '2019-08-28 17:52:02', '2019-08-28 17:52:02', '2020-08-28 19:52:02'),
('6ab4f737b54bab473b2e0672fde59f4b35c6864b6413a5c35c61c4657afd5350c8a6f1563a9369bd', 321, 3, 'MyApp', '[]', 0, '2019-07-07 11:46:53', '2019-07-07 11:46:53', '2020-07-07 13:46:53'),
('6acfa038d3e8631becafeb5bac7460e181d5ef3cf1801cf7e1d6b6e0a572ecd413a843708f41b5e7', 264, 3, 'MyApp', '[]', 0, '2019-08-20 15:55:05', '2019-08-20 15:55:05', '2020-08-20 17:55:05'),
('6af493e3674b1882da3af5c3c55cd2d69d51720548f79433a6a4d86a8dbbcb6485becbd0a4aae72e', 266, 3, 'MyApp', '[]', 0, '2019-08-20 16:02:38', '2019-08-20 16:02:38', '2020-08-20 18:02:38'),
('6ccfb22fa44fc72bea5a1f03ea015560e85f1b9f49c109cae851fe7985dec570476aabec4bf34651', 204, 1, 'MyApp', '[]', 0, '2019-06-28 21:36:47', '2019-06-28 21:36:47', '2020-06-28 23:36:47'),
('6e527b9f2f8f659dae8c8f6c1cb4d14f7e59a08c8a547485b5e179ef4202eb9e6bab79a49e87008f', 264, 3, 'MyApp', '[]', 0, '2019-07-05 19:53:56', '2019-07-05 19:53:56', '2020-07-05 21:53:56'),
('73d44e94462b1823493181936d95621873b2f36ea98fc199fce023e4a0da243e39774255de19c7dd', 264, 3, 'MyApp', '[]', 0, '2019-07-06 09:54:46', '2019-07-06 09:54:46', '2020-07-06 11:54:46'),
('73e2624d4dc2e57a8e18ff331b71c22a17c87e766a8f3fb64f30da499a2b653b1c33d5c723386d21', 303, 3, 'MyApp', '[]', 1, '2019-08-22 16:10:53', '2019-08-22 16:10:53', '2020-08-22 18:10:53'),
('78adecff874587eb9974a2f5218d7629f588b1f538249a66d123baf0616af14345a23dbd0361f45c', 264, 3, 'MyApp', '[]', 0, '2019-08-21 18:44:42', '2019-08-21 18:44:42', '2020-08-21 20:44:42'),
('79c3bb776adc466b7f850e36621cbd21fc9b6c101e135b7c3755f5ac87f2219e314e5e77c484674e', 81, 1, 'MyApp', '[]', 0, '2019-06-28 21:01:57', '2019-06-28 21:01:57', '2020-06-28 23:01:57'),
('7abdd2de68377384a256b3e4036cb41f3b793e5990a59824d197352c362f3916a745a577ae938638', 274, 3, 'MyApp', '[]', 1, '2019-07-27 12:32:22', '2019-07-27 12:32:22', '2020-07-27 14:32:22'),
('7b45fab42ad21f59378bf038ddf082b2f04d9fb8e8a71a6528ee0e0055d44554ca5e5ef2041bdd2d', 320, 3, 'MyApp', '[]', 0, '2019-07-06 10:03:18', '2019-07-06 10:03:18', '2020-07-06 12:03:18'),
('7d35e8c8c468c91ae3e870694e240f749df1cf68c6d807a0a0d9d01b61ac81dba20957b694c82279', 314, 3, 'MyApp', '[]', 0, '2019-07-02 12:58:30', '2019-07-02 12:58:30', '2020-07-02 14:58:30'),
('89a42eec12d576a0960702822fa7f6183cc0b7de380253baa6fdfe8d3026f387a4890388587b63e4', 266, 3, 'MyApp', '[]', 0, '2019-08-20 19:48:35', '2019-08-20 19:48:35', '2020-08-20 21:48:35'),
('8b2e7ae11a451430c974bd91bd2e49514a794be252e164fa27fff3f05ce7edc1da926d826de39acd', 274, 3, 'MyApp', '[]', 1, '2019-07-27 15:26:06', '2019-07-27 15:26:06', '2020-07-27 17:26:06'),
('8ec30a8ea66aa3c7bb871859a849c17e5ed453ef5aa7f8f49719f6cc6e6db5f769833888e208918c', 264, 3, 'MyApp', '[]', 0, '2019-08-20 15:53:46', '2019-08-20 15:53:46', '2020-08-20 17:53:46'),
('913965a9d09c386d9133b9a6efea73f88e321b0d170329bfe83b8db10c93a762216ea9bc5bdc4171', 264, 3, 'MyApp', '[]', 1, '2019-07-06 11:06:16', '2019-07-06 11:06:16', '2020-07-06 13:06:16'),
('92add9111f7b300372145e3de792f50e6c68b6ee3cc57f9b751678f45f79d05260cfa11c51dd93a6', 315, 3, 'MyApp', '[]', 0, '2019-07-02 13:00:13', '2019-07-02 13:00:13', '2020-07-02 15:00:13'),
('94d74927427490dddc447211c0a26f5abbcd8b14c144145c2ff975c4f56cc99dc35e1aca167ab0dd', 264, 3, 'MyApp', '[]', 0, '2019-07-27 21:02:06', '2019-07-27 21:02:06', '2020-07-27 23:02:06'),
('957e0200ae4a3cea039723b8248a6b030aa56a227675c0e50476445f44e3e6b117ceb8e5511f05a7', 264, 3, 'MyApp', '[]', 0, '2019-07-05 20:38:01', '2019-07-05 20:38:01', '2020-07-05 22:38:01'),
('9942c22bf832f45e6828524d19dba12b43d68fbf332e17e49a019369401dedf1cf5635e086b2ceda', 264, 3, 'MyApp', '[]', 1, '2019-08-28 17:22:29', '2019-08-28 17:22:29', '2020-08-28 19:22:29'),
('99bcd1e46008c9771d8f38d67f85791ba921e429ad3565819865038483081449d9607d20b34af520', 316, 3, 'MyApp', '[]', 0, '2019-07-02 13:00:40', '2019-07-02 13:00:40', '2020-07-02 15:00:40'),
('9a0c28b27ea21e2f981f680896fe0e4a8abc62edaed1c2fe3b19b37edc8dbc0863c1034e56899701', 264, 3, 'MyApp', '[]', 0, '2019-07-03 12:18:03', '2019-07-03 12:18:03', '2020-07-03 14:18:03'),
('9b4de694c5fd3ce62bdcfb6073d6ffc3e153e09d7b7db4bd5938332c833a9305d4c7039485c515ed', 264, 3, 'MyApp', '[]', 0, '2019-08-20 20:05:52', '2019-08-20 20:05:52', '2020-08-20 22:05:52'),
('9dba0f4fd1bbd05bd1e2eb0b1d503cbf0ec41c02b61a106c14d1a0f67fd094e0f86a45fea438ca4d', 322, 3, 'MyApp', '[]', 0, '2019-08-21 14:53:38', '2019-08-21 14:53:38', '2020-08-21 16:53:38'),
('a1aa3ed71a6ba5bdae665d0f28b763cc94ae1263bb52ae6efe5603c417acb6213d7b5d3f0b025cbf', 264, 3, 'MyApp', '[]', 0, '2019-07-06 09:52:43', '2019-07-06 09:52:43', '2020-07-06 11:52:43'),
('a1b624fdbe3f92e8e6f85ededb407e53df644d657e2cf9c45685549d444ba00314b33512d37f403a', 264, 3, 'MyApp', '[]', 0, '2019-07-01 18:08:23', '2019-07-01 18:08:23', '2020-07-01 20:08:23'),
('a39619f30b898d80129e87df721dcb47f57da664c3e92388ea929321d16bbd6b0ba5e38ed4f92f53', 320, 3, 'MyApp', '[]', 0, '2019-07-06 10:00:35', '2019-07-06 10:00:35', '2020-07-06 12:00:35'),
('a461e372e37a1f1512a5032b45a0cc9ed63374eb791aa526a56900fd2728315dcdeb3b00ee6ba347', 264, 3, 'MyApp', '[]', 0, '2019-07-03 21:09:12', '2019-07-03 21:09:12', '2020-07-03 23:09:12'),
('a9aa8c6b0ab8d34aa13cc716e0b38657c7f6e057f7eba4e2dd884f3432b89cc2e03fa3309d297694', 264, 3, 'MyApp', '[]', 0, '2019-08-22 14:54:07', '2019-08-22 14:54:07', '2020-08-22 16:54:07'),
('aae4c81600541224441e28e52700de76ba51de2c5403406119151c625de8219767593ed3e2c30a64', 264, 1, 'MyApp', '[]', 0, '2019-06-28 21:43:40', '2019-06-28 21:43:40', '2020-06-28 23:43:40'),
('ad169f3e724f7d47f69e2e362519210d682086338245e6f1716aead4fc942cfa8c5605ff19e458e5', 313, 3, 'MyApp', '[]', 0, '2019-07-02 12:57:58', '2019-07-02 12:57:58', '2020-07-02 14:57:58'),
('ad6baf30b5312f39b356d062eff96997f1bf187b8339247ff12f52f134d1dd613dc73d4eb66bf4a4', 264, 3, 'MyApp', '[]', 0, '2019-08-20 20:03:33', '2019-08-20 20:03:33', '2020-08-20 22:03:33'),
('aed00c5829c6e90217946220602ef2aab9d0bf07f99b15b98eba4f5ae3a62752d0631844f81ddcaf', 320, 3, 'MyApp', '[]', 1, '2019-07-06 10:59:50', '2019-07-06 10:59:50', '2020-07-06 12:59:50'),
('afc632a42e7329631cce166e708919fc16b2beb11daeed157ea4e470366d9b98bc3026f3b7d07c09', 264, 3, 'MyApp', '[]', 0, '2019-07-03 20:37:18', '2019-07-03 20:37:18', '2020-07-03 22:37:18'),
('b171e0b80c1c010caa5aa6585f0ccd08966fb75a1dadcd6581954b21214e0cb951a785860db78588', 266, 3, 'MyApp', '[]', 0, '2019-08-20 16:03:34', '2019-08-20 16:03:34', '2020-08-20 18:03:34'),
('b5fe05bdbe8c31de5950453f4d1a4afe83ae88d6bcee4cead46f651ba6a1abab282a52ab2d6dc649', 264, 3, 'MyApp', '[]', 0, '2019-07-03 13:37:26', '2019-07-03 13:37:26', '2020-07-03 15:37:26'),
('bda55b825856690ed6b9a2127f4825d2750e8d52ee9a45a944bde3d39506bb29d7256707f170a7f1', 264, 3, 'MyApp', '[]', 0, '2019-07-06 07:15:27', '2019-07-06 07:15:27', '2020-07-06 09:15:27'),
('be8c72afd2e2162c8eb84239cf29666419557132aaa6432d22583606c2b3dd0492d7215b6acb74ac', 81, 1, 'MyApp', '[]', 0, '2019-06-28 21:02:25', '2019-06-28 21:02:25', '2020-06-28 23:02:25'),
('bffe092ecfca47c59f5ff478e31662dbf5ddcaa7c88de2ca0bc44ec0fad585c3f9abd918bf8d39d9', 264, 1, 'MyApp', '[]', 0, '2019-06-29 11:34:12', '2019-06-29 11:34:12', '2020-06-29 13:34:12'),
('c1ee91416d24e5de4db71b23697698eb2cb01a7444b3790b64d35ad937760b4f1ca61c98a80ee552', 264, 1, 'MyApp', '[]', 0, '2019-06-28 21:57:49', '2019-06-28 21:57:49', '2020-06-28 23:57:49'),
('c21938de7b8baaed7f77e7f8a5a4786258dfdcf91ccf055c5aab4731e114b66a345ef52fc25321c1', 320, 3, 'MyApp', '[]', 0, '2019-07-06 09:59:18', '2019-07-06 09:59:18', '2020-07-06 11:59:18'),
('c25e4183d35ddf5aa408ff1bab17d7b83522f47280da220f2015dc6112720ce5905c9a997365c0ef', 264, 3, 'MyApp', '[]', 0, '2019-08-20 20:04:23', '2019-08-20 20:04:23', '2020-08-20 22:04:23'),
('c850bee2a7168da01ac4fe5a2b82912cf0a2c5433b8ea6a22674e86825fff21305c36c744bf7d2d8', 264, 3, 'MyApp', '[]', 0, '2019-08-20 15:54:55', '2019-08-20 15:54:55', '2020-08-20 17:54:55'),
('ca079d17498691d1266308ba74f25433c9afc22a798b51c4836728b5105e94b62daa266ec8dcf8b4', 264, 3, 'MyApp', '[]', 0, '2019-07-06 07:37:36', '2019-07-06 07:37:36', '2020-07-06 09:37:36'),
('cf1d3bbe061fa7ef712d3913a679f330d18357dfd2094d2ab2b2468c004543bfad4111490525d96e', 274, 3, 'MyApp', '[]', 1, '2019-07-27 11:39:54', '2019-07-27 11:39:54', '2020-07-27 13:39:54'),
('d0826a7d788af61c4aedd80495297506138cc02a767ead791bdbe26acc3105bd5e814859a30c8bc5', 264, 3, 'MyApp', '[]', 0, '2019-07-06 09:06:06', '2019-07-06 09:06:06', '2020-07-06 11:06:06'),
('d137c320b45c7a20abde46dad5a588e7b1c5bd682a60ccaa167391aed8357b49a25b92ad401dc828', 264, 1, 'MyApp', '[]', 0, '2019-06-29 11:38:43', '2019-06-29 11:38:43', '2020-06-29 13:38:43'),
('d42920045d072e86ea80efcef988aea43fbbb2364b128437f2a748d613b67e395ba1f512a7a6bf0d', 320, 3, 'MyApp', '[]', 0, '2019-07-06 10:02:37', '2019-07-06 10:02:37', '2020-07-06 12:02:37'),
('d4a87d92b86d55de43a78e5939df267cf9712b84236526a1459b1b45b3ef01a2ebbd76855fafd251', 310, 3, 'MyApp', '[]', 0, '2019-07-02 12:50:29', '2019-07-02 12:50:29', '2020-07-02 14:50:29'),
('d55d4f8fe7fecc79da259f0e6b74b57745caca27c23ac29feffd076ebf1e3104ae2e154f1b7b804c', 264, 3, 'MyApp', '[]', 0, '2019-08-22 15:22:55', '2019-08-22 15:22:55', '2020-08-22 17:22:55'),
('d77f9b49674f5a928b84deeef9196f63b645b91743f082050ed49f9869053f5fe8ae105e3b577e83', 320, 3, 'MyApp', '[]', 0, '2019-07-06 10:06:45', '2019-07-06 10:06:45', '2020-07-06 12:06:45'),
('d8bc66bcd017fcf0941d84ee03707048696fa6e814ea18d99f754c379dea25e95e743bdb532a6b70', 264, 3, 'MyApp', '[]', 0, '2019-07-03 21:31:01', '2019-07-03 21:31:01', '2020-07-03 23:31:01'),
('d90535f35bf046e2c45246309bb8dc659c975ea1e3693ca7cdcf9878d04c496dd3808172c8e2d073', 264, 3, 'MyApp', '[]', 0, '2019-07-03 13:38:08', '2019-07-03 13:38:08', '2020-07-03 15:38:08'),
('db9a9aca6c2d0339c2117cfb99ca1df8e83a227f9dfa457ddd1f8e2673370ddfcc0bcc87deca8048', 264, 3, 'MyApp', '[]', 0, '2019-07-03 21:43:56', '2019-07-03 21:43:56', '2020-07-03 23:43:56'),
('dccee26c90ae22b6ab81b8efee831fd7deb6dff4af33496b1fb8b2d3636eb92955d8d632db46c64c', 306, 3, 'MyApp', '[]', 0, '2019-07-02 12:33:18', '2019-07-02 12:33:18', '2020-07-02 14:33:18'),
('df3bcf6cc3d42087c3eb8a795f6b45bd1deaf164e9b974c79f501ef3c3430764d77530160b7bfe8f', 264, 3, 'MyApp', '[]', 0, '2019-07-05 20:48:48', '2019-07-05 20:48:48', '2020-07-05 22:48:48'),
('e0696fda9436822c6e22844b24ad70edcb3d785d8cecef8ac1bd121b09d214ba7b854025ee970271', 264, 3, 'MyApp', '[]', 1, '2019-07-06 16:21:31', '2019-07-06 16:21:31', '2020-07-06 18:21:31'),
('e500d7d25491cb5a67ad1684634152b7042c93469b531589e0cd2f1fe143e2b362c2fecef43da66a', 264, 3, 'MyApp', '[]', 0, '2019-07-03 12:41:22', '2019-07-03 12:41:22', '2020-07-03 14:41:22'),
('e69800884cec77d179d9d5d25d2e0c1b3c08df1d0a8d3c34ac9696d11d7e92ba16f4416ef47a00c2', 303, 1, 'MyApp', '[]', 0, '2019-06-28 21:43:16', '2019-06-28 21:43:16', '2020-06-28 23:43:16'),
('e8a82a5c0ad1310c34f8d1e5a81663be8be725cec914dbfc8e9708b10fcdd8aec764e17c4b6362b0', 320, 3, 'MyApp', '[]', 0, '2019-07-06 10:05:15', '2019-07-06 10:05:15', '2020-07-06 12:05:15'),
('e92bb015f8cba64898d2f3d97397650af1625fba729f5112403a84ec6b808ea13fbe4e6b3e5befcc', 264, 1, 'MyApp', '[]', 0, '2019-06-29 11:36:05', '2019-06-29 11:36:05', '2020-06-29 13:36:05'),
('eac6e4c60091709382a0e2f622a5e22db86f7d36d856dda96436279a6b60af3ea0e7f0023f90decc', 264, 3, 'MyApp', '[]', 1, '2019-07-06 16:18:47', '2019-07-06 16:18:47', '2020-07-06 18:18:47'),
('eafd808081fbe8e3d0be2bf7ac4541f3d4e0b76674d01311a9653937cb2753201a8abcb93e85d639', 264, 3, 'MyApp', '[]', 0, '2019-07-06 10:34:11', '2019-07-06 10:34:11', '2020-07-06 12:34:11'),
('ee5f60b5c330f4c04b929c65709c930cb6e2233faf1457ea044841021513ed00139980aed18db7a8', 264, 3, 'MyApp', '[]', 0, '2019-08-22 15:13:31', '2019-08-22 15:13:31', '2020-08-22 17:13:31'),
('ef6a46ac3e182e2c9d14c6169a4acb806fbfc7172aaf00e5f5d91d8fa7267d1cf63e154ad829fb0d', 264, 3, 'MyApp', '[]', 0, '2019-08-20 20:02:45', '2019-08-20 20:02:45', '2020-08-20 22:02:45'),
('ef7bc6e1479dd51e9c6fad473ededb25bd87bff4fbdcd1b2c0b341f71de3dfff83f6060bdb139bcc', 264, 1, 'MyApp', '[]', 0, '2019-06-29 07:51:36', '2019-06-29 07:51:36', '2020-06-29 09:51:36'),
('f0d637b81c0d8ee7f4cafe1ba4c02e6c8014bc940dfb9d9499545a40c45b92167f1df3e54771a246', 264, 3, 'MyApp', '[]', 0, '2019-07-06 09:43:35', '2019-07-06 09:43:35', '2020-07-06 11:43:35'),
('f298c5991fd1ec64270cc8c981195b5e0892e91f6a24db5dc7815ba70a485e9011f211c1403bc912', 264, 1, 'MyApp', '[]', 0, '2019-06-29 11:33:59', '2019-06-29 11:33:59', '2020-06-29 13:33:59'),
('f2f9de5832c06d984e449dc5cbb1e021cdeba1b51a3bbc1feed0ab4e7938145f1bd43d987f55352a', 264, 3, 'MyApp', '[]', 0, '2019-08-20 15:54:31', '2019-08-20 15:54:31', '2020-08-20 17:54:31'),
('f4ff2c8907d524b556adfbdbd5f67f13415525aebb7272faec53e2d6663c927e84e3b35a1c2f5774', 266, 3, 'MyApp', '[]', 0, '2019-08-20 19:49:16', '2019-08-20 19:49:16', '2020-08-20 21:49:16'),
('f921f6357d75cd3c04331e6dc361eec4f64fd686643be1036e3b56395f0a11e0118915f0844af1a3', 264, 3, 'MyApp', '[]', 0, '2019-07-06 10:30:17', '2019-07-06 10:30:17', '2020-07-06 12:30:17'),
('f9ec13d386e0134d28ed78eca4ad67921deb4ad44349ff2dc26803df714d1498ab2f6bf90a101f69', 264, 3, 'MyApp', '[]', 0, '2019-07-03 20:29:34', '2019-07-03 20:29:34', '2020-07-03 22:29:34'),
('fab567a343b84f37a7ff2f0bfe489c6ce8e64e0246f5fd4bff08a2bea6a3b78c26a27fc9322106d8', 320, 3, 'MyApp', '[]', 1, '2019-08-22 16:04:30', '2019-08-22 16:04:30', '2020-08-22 18:04:30'),
('fd6fc336374947268bc45856aa9dfa6474cbf476d1e26072b1b2b6c470966e0f5c1507d4d040b111', 272, 3, 'MyApp', '[]', 1, '2019-08-20 19:24:09', '2019-08-20 19:24:09', '2020-08-20 21:24:09'),
('fd87a2de33804d7f9bec91b1af822954b95d1b2803fd951fb7bd51cfa5852675db89cb47105b5414', 264, 3, 'MyApp', '[]', 0, '2019-07-06 08:46:14', '2019-07-06 08:46:14', '2020-07-06 10:46:14');

-- --------------------------------------------------------

--
-- Structure de la table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '306ByQyucoCphCqaHs5rs9Mx5akwBJq4H37roWKc', 'http://localhost', 1, 0, 0, '2019-06-28 20:51:29', '2019-06-28 20:51:29'),
(2, NULL, 'Laravel Password Grant Client', 'irDceODstcQa3JqSkRQ89MxoW6wX8kUN17o9S7F6', 'http://localhost', 0, 1, 0, '2019-06-28 20:51:29', '2019-06-28 20:51:29'),
(3, NULL, 'Laravel Personal Access Client', 'xrUPGELPKGPw5ez7FwSEKc88tlkeiZ7Wp0bEACwn', 'http://localhost', 1, 0, 0, '2019-07-01 17:41:12', '2019-07-01 17:41:12'),
(4, NULL, 'Laravel Password Grant Client', 'vBqCdIrVpCLvafkMP2GIacNeHpUwBWCf2rzGQIxL', 'http://localhost', 0, 1, 0, '2019-07-01 17:41:12', '2019-07-01 17:41:12');

-- --------------------------------------------------------

--
-- Structure de la table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-06-28 20:51:29', '2019-06-28 20:51:29'),
(2, 3, '2019-07-01 17:41:12', '2019-07-01 17:41:12');

-- --------------------------------------------------------

--
-- Structure de la table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `present` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `imei` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mac` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `lastname`, `firstname`, `email`, `email_verified_at`, `password`, `notification_key`, `present`, `remember_token`, `created_at`, `updated_at`, `admin`, `imei`, `mac`) VALUES
(264, 'Admin', 'Admin', 'admin@admin.com', NULL, '$2y$10$nED07mE2p6SaCQCWkG/g3egd./VhuF8HhIMHvMWdaZytZxUc/d4HK', '', 1, NULL, '2019-06-28 21:41:37', '2019-08-20 15:55:05', 1, NULL, NULL),
(265, 'David', 'David', 'seguin.catherine@orange.fr', NULL, '$2y$10$jdzUPo3xV8vhzdAqD2aDN.hGi75zW9qZgTYx77lo0iURX1EE5cZuW', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(266, 'Roux', 'Émile', 'arthur.noel@huet.fr', NULL, '$2y$10$YZ3KASeY7M8yuMKhvlM4H.vl2sxI261Iw7tFWF192DMl7MvIqFm6K', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(267, 'Hamon', 'Henri', 'bjulien@wanadoo.fr', NULL, '$2y$10$h3Ren4meKz/WQel2Erf7.uZ3SegmwwsqzFi5nAD2BPKDkncAuroby', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(268, 'Hamel', 'Dominique', 'laurent19@laposte.net', NULL, '$2y$10$RHB1.D3PaO8Lb7lsYEsUreeQIMg4lJ0M4IuWiaYvFa9s7iyyn8Ulu', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(269, 'Regnier', 'Denise', 'bmary@laposte.net', NULL, '$2y$10$HqU/eO9AANT3uE5QeE84lOVKltUwSU3jqcMvuEs8B.l3P9qFSvC8m', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(270, 'Rocher', 'Richard', 'marie.pauline@ferrand.fr', NULL, '$2y$10$VTcFnxVELzQqCT4RlbdJg.Ha.zH/ZMEINi/qojFiKHgWqyKQm3zd6', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(271, 'Mendes', 'Victoire', 'guillaume.frederic@orange.fr', NULL, '$2y$10$VfW1O21HD.kOvGCeSz3S4.IitGwy1oJCXW8mbZiTzx1GtNGfrGfJG', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(272, 'Le Roux', 'Guy', 'agnes44@jean.fr', NULL, '$2y$10$ZmwIpxyCoq/yx06zMFs/7O5O7mQOXZxKHbUo/XH2c2tqkK0H8h4dy', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(273, 'Blanchet', 'Benoît', 'jean44@tele2.fr', NULL, '$2y$10$ptETSYA5XMg4IOh9iqy93u/r0twkMW0x0aiVwoNi57Yi3T.duRNMO', '', 0, NULL, '2019-06-28 21:41:37', '2019-06-28 21:41:37', 1, NULL, NULL),
(274, 'Marie', 'Christine', 'student@student.com', NULL, '$2y$10$.Jv.KrqIOfv2Zj28eXoDFunkMk8MT0iYO1eg6iiIgJPXdV6h7fThe', '', 1, NULL, '2019-06-28 21:41:41', '2019-07-27 20:36:09', 0, NULL, NULL),
(275, 'Verdier', 'Claude', 'potier.roger@bouygtel.fr', NULL, '$2y$10$FDjzaV/SI.AQyU2Pp7VxbObmSAokzGI9I8JIaGH9HKWJQWwvPjOLu', '', 0, NULL, '2019-06-28 21:41:41', '2019-06-28 21:41:41', 0, NULL, NULL),
(276, 'Schneider', 'Emmanuel', 'benoit.lacroix@aubert.com', NULL, '$2y$10$7tNa3cH0nNQQrcaASqLe8OI4SNm.vruJ/WcQ4.MDOaglJP/K7nsAa', '', 0, NULL, '2019-06-28 21:41:41', '2019-06-28 21:41:41', 0, NULL, NULL),
(277, 'Bousquet', 'Josette', 'alain26@free.fr', NULL, '$2y$10$d8DFG667WiUw0vND07xnzOskqhOARhr6iGfmi0rEPhWMEixb69N0G', '', 0, NULL, '2019-06-28 21:41:41', '2019-06-28 21:41:41', 0, NULL, NULL),
(278, 'Leduc', 'Gabriel', 'rene68@dasilva.com', NULL, '$2y$10$glXCx1CTnIOk88e7N2M4c.Fa38V5aSVmcUiK4ze8IQPTKhgEjBS.S', '', 0, NULL, '2019-06-28 21:41:41', '2019-06-28 21:41:41', 0, NULL, NULL),
(279, 'Francois', 'Lucie', 'jmenard@free.fr', NULL, '$2y$10$KVSBlVsSYawqqg.2oapuie//eFcmtFO0JieuqQsyX99UDFG2354cO', '', 0, NULL, '2019-06-28 21:41:41', '2019-06-28 21:41:41', 0, NULL, NULL),
(280, 'Hardy', 'Benjamin', 'georges.barre@diaz.com', NULL, '$2y$10$foeGuGWV3AwQ/AYC7paV1.O4LiXHr.iW5/sfTLWhSySonlFQBzP9K', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(281, 'Lebrun', 'Noémi', 'tleleu@texier.net', NULL, '$2y$10$DmuG.6aENHuFuBGlQeFbjeV8Gf8lBLX2imxPSeO6PYzDhbRBEA9Xi', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(282, 'Rossi', 'Guillaume', 'henri16@live.com', NULL, '$2y$10$z31JyJFf6gjSR.Br3VX3RuDV3iCiLV5JBd8Bz2co7jQERlEZiXJGe', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(283, 'Vaillant', 'Marthe', 'duhamel.theodore@noos.fr', NULL, '$2y$10$yLZOk5d.u3pKsabuKuq/neE4c1qTamh2sEqLPPWwb79kooy1XqxFO', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(284, 'Collin', 'Madeleine', 'qleroy@hoareau.fr', NULL, '$2y$10$IOKL.JV3YLMUSTlIXkDsru5Th4Ue/QikOguC65kAQVn2/reYhSMt2', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(285, 'Morin', 'Sébastien', 'berthelot.agathe@renard.net', NULL, '$2y$10$nbb0pjYOe1IlvQ51DaiROO3Leo9oP54tU2ejp1Qg0jH2.uQyUXe/m', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(286, 'Alexandre', 'Dominique', 'juliette.raynaud@robin.fr', NULL, '$2y$10$hnrpKSVRojO2ywfuYfAz1eSyf04WFC.lma1anLUuv4n4MkUFw1mDC', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(287, 'Torres', 'François', 'alain49@bouygtel.fr', NULL, '$2y$10$97wBESpi7x0G.cFEOc/aZ.I85C2hPF9xCq6wQ5y5mCTRv4L1vlXCm', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(288, 'Andre', 'Roland', 'gauthier.charlotte@tiscali.fr', NULL, '$2y$10$SBaKAVGArw8bWWb/BHYb0eREHyrqho//aEmyeP725X5L4MLZJz1gG', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(289, 'Lemaitre', 'Hugues', 'nboyer@poirier.com', NULL, '$2y$10$jCYzJDGyYjbHHxFKdQiRyOdnV/YMRQDREyKY5H8w3f.NhcrbLTTAu', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(290, 'Mahe', 'Gabriel', 'laure79@hotmail.fr', NULL, '$2y$10$nkQIYs6ap06PhcF8lDYcnOET6ZrNukQF5N4nRwNN/9sF0UG.1j.7u', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(291, 'Guillot', 'Alexandre', 'kparent@leger.com', NULL, '$2y$10$JdBFN0ftA.5lP73I40GGeeyCDJFjrux96qhs5Tvtcu5FM8OPHqdKS', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(292, 'Leconte', 'Brigitte', 'masson.eugene@dacosta.org', NULL, '$2y$10$AzNqb/cv9GQPHWaK8sgmKuledIW3VpJl.T9gpGjzhSNjdnO4tZsQu', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(293, 'Schmitt', 'Christelle', 'hbernier@gmail.com', NULL, '$2y$10$2HuORfU6Xo.AbyrDiH316u6DuNlw29Y0LzrGxp1K1kYSMFAU5d9TG', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(294, 'De Sousa', 'Robert', 'qboutin@guilbert.org', NULL, '$2y$10$5ucr2R4kUZPqR2BIDlHEMeR3QDxfHBiuOe25nvsjq8tgDYqLfg65e', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(295, 'Munoz', 'Emmanuel', 'lorraine.navarro@hotmail.fr', NULL, '$2y$10$E.ZROHlYCcQx3bxd0iiv0.bW992V9b1amzfkQe7th3zSf8OBGDoni', '', 1, NULL, '2019-06-28 21:41:42', '2019-08-22 16:18:36', 0, NULL, NULL),
(296, 'Lefebvre', 'Adèle', 'gilbert.besson@tiscali.fr', NULL, '$2y$10$zC7XKw5rZPLHa06rQPky2OQYRvfcsk30ZXDAHhkjoIW3t22puyFlC', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(297, 'Perrier', 'Marianne', 'marianne59@noos.fr', NULL, '$2y$10$S0ggzVpnZFr.8dcUX4jDxOl2Ns9QSnCD6dgji/dcLseB4FoxhC7QS', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(298, 'Levy', 'Sophie', 'marianne.prevost@olivier.net', NULL, '$2y$10$EeBXjbEA8WYezvRdHP/OruE0oEomCUJUGYomhUIi9nVPBhVemiM1a', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(299, 'Dupont', 'André', 'marianne57@laposte.net', NULL, '$2y$10$nB/03z8UwpkpQesYX/miJuHlxCZ/k0W2BZXd.nyQbLfE6qUxh2Ykq', '', 0, NULL, '2019-06-28 21:41:42', '2019-06-28 21:41:42', 0, NULL, NULL),
(300, 'Nguyen', 'Camille', 'clerc.claire@bigot.com', NULL, '$2y$10$c/D/sSGzlIHck9GJaakUCOl.hCW9oH5q.MzSUbtTxmrnblyyF0KJm', '', 0, NULL, '2019-06-28 21:41:43', '2019-06-28 21:41:43', 0, NULL, NULL),
(301, 'Hernandez', 'Dominique', 'zoe.foucher@mallet.com', NULL, '$2y$10$LmanQnUHezcCEmMsTTmjHu5RinoeS5Cvl3yKLOX/GMdDw0mMrbM6q', '', 0, NULL, '2019-06-28 21:41:43', '2019-06-28 21:41:43', 0, NULL, NULL),
(302, 'Roussel', 'Joséphine', 'xklein@labbe.org', NULL, '$2y$10$ZzD.rSkf.g6ko9ENvpWg3ejRN2KE.zZ4hQP38ezpRt8qEU96iOjVy', '', 0, NULL, '2019-06-28 21:41:43', '2019-08-21 19:12:07', 0, NULL, '456'),
(303, 'Joly', 'Jérôme', 'vleroux@chauvin.com', NULL, '$2y$10$M14Q0uxEr/DXy3IhmgwzzeROfBLYmOapPqTPLZ3SivGDXR/2jjTVe', '', 1, NULL, '2019-06-28 21:41:43', '2019-08-22 16:12:04', 0, NULL, '02:00:00:00:00:00'),
(315, 'Admin', 'Admin', 'admin@admin.fr', NULL, '$2y$10$WsQubYCoY5zmvIIWcL6LmuHY.EYKs1bJ3iV8a9VdMA354GRS16Pty', NULL, 0, NULL, '2019-07-02 13:00:13', '2019-07-02 13:00:13', 1, NULL, NULL),
(318, 'Admin', 'Admin', 'admin@admin.net', NULL, '$2y$10$vF.1SPS1EbLRoUiSzrw7l.vBiiiUz82lXbblNvYkfabxXeFER7rvK', NULL, 0, NULL, '2019-07-03 13:41:48', '2019-07-03 13:41:48', 1, NULL, NULL),
(319, 'Garnier', 'Nicolas', 'jerbi@admin.com', NULL, '$2y$10$j5juZZW13XXBKj1pJK0lXeCFQishaQecYHyAEi8d4o0gqrYUiCvDO', NULL, 0, NULL, '2019-07-05 17:18:48', '2019-07-05 17:18:48', 0, NULL, NULL),
(320, 'sponge', 'bob', 'spongebob@student.com', NULL, '$2y$10$ZjQkSOUWVNTDLZ0EHLcunOlGfupLnGEPU33IyeB5HiF9blYNO/j96', NULL, 1, NULL, '2019-07-06 09:59:18', '2019-08-22 16:07:10', 0, NULL, NULL),
(321, 'raspberry', 'pi', 'raspberrypi@pi.com', NULL, '$2y$10$1z2jpM8bMJatWiQ0ziPjjuTR10mBtkTs/KfxE1AcY51eKyZAC0afi', NULL, 0, NULL, '2019-07-07 11:46:53', '2019-08-20 19:43:13', 2, NULL, NULL),
(322, 'Test', 'Test', 'aloha@gmail.com', NULL, '$2y$10$aTJP3IoHuCrY6lv70c6dEOluaVCmBtCg/KAz8Xk/8ZvkkvvhCI6mO', NULL, 0, NULL, '2019-08-21 14:53:38', '2019-08-21 14:53:38', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `websockets_statistics_entries`
--

CREATE TABLE `websockets_statistics_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int(11) NOT NULL,
  `websocket_message_count` int(11) NOT NULL,
  `api_message_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_user_id_foreign` (`user_id`);

--
-- Index pour la table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `grade_course`
--
ALTER TABLE `grade_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade_course_grade_id_foreign` (`grade_id`),
  ADD KEY `grade_course_course_id_foreign` (`course_id`);

--
-- Index pour la table `grade_user`
--
ALTER TABLE `grade_user`
  ADD KEY `grade_user_grade_id_foreign` (`grade_id`),
  ADD KEY `grade_user_user_id_foreign` (`user_id`);

--
-- Index pour la table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Index pour la table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT pour la table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `grade_course`
--
ALTER TABLE `grade_course`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=323;

--
-- AUTO_INCREMENT pour la table `websockets_statistics_entries`
--
ALTER TABLE `websockets_statistics_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `grade_course`
--
ALTER TABLE `grade_course`
  ADD CONSTRAINT `grade_course_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `grade_course_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`);

--
-- Contraintes pour la table `grade_user`
--
ALTER TABLE `grade_user`
  ADD CONSTRAINT `grade_user_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`),
  ADD CONSTRAINT `grade_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
