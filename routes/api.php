<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Public routes
Route::post('login_imei', 'UsersController@login_imei');
Route::post('login', 'UsersController@login');
Route::post('register', 'UsersController@register');

Route::group(['middleware' => 'auth:api'], function (){
  Route::post('logout', 'UsersController@logout');
  Route::get('students/{id}','StudentsController@show');
  Route::get('courses/{id}','CoursesController@show');
  Route::get('grades/{id}','GradesController@show');
  Route::put('presentBluetooth', 'UsersController@presentBluetooth');
  Route::put('presentQr', 'UsersController@presentQr');
  Route::get('qrcode','QrCodesController@qrcode');
});