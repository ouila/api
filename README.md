# Installation


1) Déplacez vous dans le dossier du projet

2) ( Cette étape nécessite [Composer](https://getcomposer.org/) ) : 
```
composer install
```
3) Lancez votre serveur web (par exemple: XAMPP)

4) Créer une base de données sur votre serveur web en lui donnant pour nom : *db_ouila*

5) Importer le fichier *db_ouila.sql* dans la base de données

6) Copier le fichier d'environnement
```
cp .env.example .env
```
7) Modifier le fichier *.env* en y mettant les informations de votre base de données et utilisateur

*Par exemple:*
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_ouila
DB_USERNAME=root
DB_PASSWORD=""
```
8) Modifier la partie concernant les websockets pour obtenir ceci

```
BROADCAST_DRIVER=redis
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120
```

9) Installer et lancer le serveur REDIS 

- Si vous êtes sur Windows suivez ces indications: https://www.supinfo.com/articles/single/8843-redis-installation-windows
- Si vous êtes sur Linux suivez ces indications: https://redis.io/topics/quickstart

10) Lancer ces commandes

```
npm install

npm install -g laravel-echo-server
```
11) Démarrer le laravel-echo-server

```
laravel-echo-server start
```

12) Générer une clé de cryptage

```
php artisan key:generate
```

13) Générer clé public/privé

```
php artisan passport:install
```

14) Démarrer le serveur
```
php artisan serve
```
Si le 10 ne marche pas : ```php -S 127.0.0.1:8000 -t public/```

15) Ouvrez ce lien: [http://127.0.0.1:8000](http://127.0.0.1:8000) pour visualiser l'API sur votre navigateur web.

