<?php

use Illuminate\Database\Seeder;
use App\User;
class StudentsUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i=0; $i < 30; $i++){
            $password = bcrypt('student');
            User::create([
                'lastname' => $faker->lastName(null),
                'firstname' => $faker->firstName(null),
                'email' => $faker->email,
                'password' => $password,
                'notification_key' => "",
                'admin'=> false
            ]);
        }
    }
}
