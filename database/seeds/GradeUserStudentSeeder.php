<?php

use Illuminate\Database\Seeder;

class GradeUserStudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 30; $i++) {
            DB::table('grade_user')->insert([
                'grade_id' => $faker->numberBetween(1, 15),
                'user_id' => $faker->numberBetween(274, 303)
            ]);
        }
    }
}
