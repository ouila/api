<?php

use Illuminate\Database\Seeder;
use App\User;
class TeachersUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i=0; $i < 10; $i++){
            $password = bcrypt('root');
            User::create([
                'lastname' => $faker->lastName(null),
                'firstname' => $faker->firstName(null),
                'email' => $faker->email,
                'password' => $password,
                'notification_key' => "",
                'admin'=> true
            ]);
        }

    }
}
