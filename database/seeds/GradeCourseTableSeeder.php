<?php

use Illuminate\Database\Seeder;

class GradeCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 15; $i++) {
            DB::table('grade_course')->insert([
                'grade_id' => $faker->unique()->numberBetween(1, 15),
                'course_id' => $faker->numberBetween(21, 40)
            ]);
        }
    }
}
