<?php

use Illuminate\Database\Seeder;
use App\Course;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 20; $i++) {
            Course::create([
                'label' => $faker->word,
                'start_time' => $faker->dateTime('now', null),
                'duration' => $faker->dateTime('now',  null),
                'allClass' => $faker->boolean(50),
                'user_id' => $faker->numberBetween(264,273)
            ]);
        }
    }
}
