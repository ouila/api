<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grade_course', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('grade_id')->unsigned();
            $table->bigInteger('course_id')->unsigned();
            $table->foreign('grade_id')->references('id')->on('grades');
            $table->foreign('course_id')->references('id')->on('courses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grade_course');
    }
}
